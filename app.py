from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from gevent.pywsgi import WSGIServer
import httplib
import json
import logging
import os
import signal
import sys

from model import app, db
from helpers import create_event

LOG_LEVEL = os.getenv('LOG_LEVEL', 'DEBUG').upper()
LOG_FORMAT = '%(asctime)s %(levelname)s %(name)s - %(filename)s:%(lineno)s - %(message)s'

@app.route('/healthcheck')
def status():
    return json.dumps({'status': 'happy puppy'})


@app.route('/api/v1/<product_id>/events', methods=['POST'])
def events(product_id):
    event = create_event(request.get_json(force=True), product_id)

    save_event(product_id, event)
    return ('', httplib.NO_CONTENT)


def sigterm_handler(_signo, _stack_frame):
    logging.warn('Quitting')
    sys.exit(0)

def save_event(product_id, event):
    try:
        # saves the events to the database
        logging.info('Got event: {}'.format(str(event)))
        db.session.add(event)
        db.session.commit()
    except Exception as e:
        logging.exception('Unexpected error: {}'.format(sys.exc_info()))
        logging.exception(sys.exc_info()[2])


def init():
    logging.basicConfig(format=LOG_FORMAT, level=LOG_LEVEL)
    signal.signal(signal.SIGINT, sigterm_handler)
    signal.signal(signal.SIGTERM, sigterm_handler)


if __name__ == '__main__':
    init()
    port = 8000
    logging.info('Starting server on port {}'.format(port))
    http_server = WSGIServer(('0.0.0.0', port), app)
    http_server.serve_forever()
