from datetime import datetime
from model import Event

VERSION_1_1_0 = '1.1.0'

def create_event(e, product_id):
    e['version'] = VERSION_1_1_0
    e['appId'] = product_id

    eventObj = \
        Event(
            eventId = e['eventId'],
            timestamp = int(e['timestamp']),
            appId = int(product_id),
            platform = e['platform'],
            name = e['name'],
            sessionId = e['sessionId'],
            source = e['source'],
            target = e['target'],
            props = str(e['props'])
        )
    return eventObj

