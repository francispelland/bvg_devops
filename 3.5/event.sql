USE devops_fp;

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventId` varchar(64) NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  `appId` int(11) NOT NULL,
  `platform` varchar(64) NOT NULL,
  `name` varchar(128) NOT NULL,
  `sessionId` varchar(64) NOT NULL,
  `source` varchar(128) NOT NULL,
  `target` varchar(128) DEFAULT NULL,
  `props` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

