General
-------
This is a simple Python web application that listens on port 8000 and collects game events.

It can be started with `python app.py`.

The application exposes two endpoints:
- GET `/healthcheck`, which responds with a hardcoded JSON blob indicating the application is up and working
- POST `/api/v1/<product_id>/events` to which events can be sent in a JSON format; the application saves events to a MySQL database

A sample request can be generated via:
```
curl -X POST \
  http://localhost:8000/api/v1/3/events \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
  "platform": "Android",
  "timestamp": "1484841494638",
  "name": "Item Purchase",
  "eventId": "0a041e2d-f9b9-4439-acb3-d879dc7b52f4",
  "sessionId": "e358fbce-5060-4e20-8076-417bcafeae38",
  "transactionId": "506f9169-f7a2-4024-969b-6d0f15007ad2",
  "source": "6d83cae6-5c44-4f4c-973a-32c53eb0c8ca",
  "sourceType": "User",
  "target": "309",
  "targetType": "Item",
  "props": {
    "cost": "1.99",
    "user_agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36"
  }
}'
```

Dependencies
------------
Python dependencies are in `requirements.txt`.

The application requires a MySQL database.
- the connection string can be updated as appropriate in `model.py`
- the schema of the `event` table is defined in the `event.sql` file

