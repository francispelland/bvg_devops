from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
#TODO: change as required
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:9IaGb6bbPegNah1e@35.188.152.202/devops'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    eventId = db.Column(db.String(64), nullable=False)
    timestamp = db.Column(db.BigInteger, nullable=False)
    appId = db.Column(db.Integer, nullable=False)
    platform = db.Column(db.String(64), nullable=False)
    name = db.Column(db.String(128), nullable=False)
    sessionId = db.Column(db.String(64), nullable=False)
    source = db.Column(db.String(128), nullable=False)
    target = db.Column(db.String(128), nullable=True)
    props = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return '<Event {} ({} @ {}>'.format(self.name, self.eventId, str(self.timestamp))
